<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<footer class="page-footer grey">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Not Aiep</h5>
                <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


            </div>


    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="orange-text text-lighten-3" href="">Efrain Garay A.</a>
        </div>
    </div>
</footer>
