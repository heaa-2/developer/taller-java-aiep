<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 20:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Objects" %>
<html>
<jsp:include page="./shared/head.jsp" />
<body>
<jsp:include page="./shared/header.jsp" />
<%
    Integer a =  Integer.parseInt(request.getParameter("number-one"));
    Integer b =  Integer.parseInt(request.getParameter("number-two"));
    Integer c =  Integer.parseInt(request.getParameter("number-three"));
    Double X1 =  - ((b) + Math.sqrt( Math.pow(b,2)- (4*(a)*(c)) ))/ (2 * (a));
    Double X2 =  - ((b) - Math.sqrt( Math.pow(b,2)- (4*(a)*(c)) )) / (2 * (a));
    
%>
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <h2 class="center-align">Resultados</h2>
        <div class="row">
            <table class="striped col s6 offset-s3">
                <tbody>
                <tr>
                    <td class="right-align ">
                        Resultado Ecuación 1:
                    </td>
                    <td class="left-align">
                        <%
                            if (Double.isNaN(X1)){
                                out.print("Sin solucion Real");
                            }else{
                                out.print(X1);
                            }
                        %>
                    </td>
                </tr>
                <tr>
                    <td class="right-align ">
                        Resultad Ecuación 2:
                    </td>
                    <td class="left-align">
                        <%
                            if (Double.isNaN(X2)){
                                out.print("Sin solucion Real");
                            }else{
                                out.print(X2);
                            }
                        %>
                    </td>
                </tr>

                </tbody>
            </table>

            <a class="col s6 offset-s3 btn waves-effect waves-light" href="math.jsp">Regresar
                <i class="material-icons right">arrow_back</i>
            </a>

        </div>


    </div>
</div>
<jsp:include page="./shared/footer.jsp" />
<!--JavaScript at end of body for optimized loading-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="./js/materialize.js"></script>
</body>
</html>
