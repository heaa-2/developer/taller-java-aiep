<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 21:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="./shared/head.jsp" />
<body>
<jsp:include page="./shared/header.jsp" />
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <div class="row">
            <div class="col s6">
                <img src="img/mira.png"  width="100%"/>
            </div>

            <form class="col s6" action="./result.jsp" method="post">
                <blockquote>
                    Despues de introducir tus datos oprime el boton "Enviar"
                </blockquote>
                <div class="row">
                    <div class="input-field col s6">
                        <input name="first_name"  placeholder="Ingrese nombre" id="first_name" type="text" class="validate">
                        <label for="first_name">Nombre</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="last_name"  placeholder="Ingrese Apellidos"  id="last_name" type="text" class="validate">
                        <label for="last_name">Apellidos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="email"  id="email" placeholder="Ingrese Email" type="email" class="validate">
                        <label for="email">Correo</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <p>
                            Eres:
                        </p>
                    </div>
                    <div class="input-field col s4">
                        <p>
                            <label>
                                <input name="sex"  value="Hombre" type="radio"  />
                                <span>Hombre</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s4">
                        <p>
                            <label>
                                <input name="sex"  value="Mujer" type="radio"  />
                                <span>Mujer</span>
                            </label>
                        </p>
                    </div>
                </div>
                <blockquote>
                    Selecciona lo que sabes.
                </blockquote>

                <div class="row">
                    <div class="input-field col s3">
                        <p>
                            <label>
                                <input name="skill[]" value="Java" type="checkbox" />
                                <span>Java</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s3">
                        <p>
                            <label>
                                <input name="skill[]"  value="C/C++" type="checkbox" />
                                <span>C/C++</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s3">
                        <p>
                            <label>
                                <input name="skill[]" value="Basic" type="checkbox" />
                                <span>Basic</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s3">
                        <p>
                            <label>
                                <input name="skill[]" value="HTML" type="checkbox" />
                                <span>HTML</span>
                            </label>
                        </p>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s6">
                        <blockquote>
                            Selecciona los idiomas que comprendes
                        </blockquote>
                        <select multiple name="languaje">
                            <option value="Español">Español</option>
                            <option value="Ingles">Ingles</option>
                            <option value="Frances">Frances</option>
                            <option value="Aleman">Aleman</option>
                        </select>

                    </div>
                    <div class="input-field col s6">
                        <blockquote>
                            Comentarios adicionales
                        </blockquote>
                        <br />
                        <textarea name="comentarios" id="textarea1" class="materialize-textarea"></textarea>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s6">
                        <button  type="reset" class="waves-effect waves-light btn"><i class="material-icons left">delete</i>Borrar</button>
                    </div>
                    <div class="input-field col s6">
                        <button type="submit" class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>Enviar</button>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
<jsp:include page="./shared/footer.jsp" />
<!--JavaScript at end of body for optimized loading-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="./js/materialize.js"></script>
<script>
    (function($){
        $(function(){

            $('.sidenav').sidenav();
            $('select').formSelect();
        }); // end of document ready
    })(jQuery); // end of jQuery name space

</script>
</body>
</html>
