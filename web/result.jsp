<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 20:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Objects" %>
<html>
<jsp:include page="./shared/head.jsp" />
<body>
<jsp:include page="./shared/header.jsp" />
<%
    String firstName = request.getParameter("first_name");
    String lastName = request.getParameter("last_name");
    String email = request.getParameter("email");
    String sex = request.getParameter("sex");
    String[] skill = request.getParameterValues("skill[]");
    String[] languaje = request.getParameterValues("languaje");


%>
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <h2 class="center-align">Validar Datos</h2>
        <div class="row">
            <table class="striped col s6 offset-s3">
                <tbody>
                <tr>
                    <td class="right-align ">
                        Nombre:
                    </td>
                    <td class="left-align">
                        <%=firstName%>
                    </td>
                </tr>
                <tr>
                    <td class="right-align ">
                        Apellidos:
                    </td>
                    <td class="left-align">
                        <%=lastName%>
                    </td>
                </tr>
                <tr>
                    <td class="right-align ">
                        Email:
                    </td>
                    <td class="left-align">
                        <%=email%>
                    </td>
                </tr>
                <tr>
                    <td class="right-align ">
                        Eres:
                    </td>
                    <td class="left-align">
                        <%=sex%>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="col s6 offset-s3">
                <p>
                    <%
                        if (skill != null && skill.length != 0) {
                            out.println("Manejas los siguientes de programación: ");
                            for (int i = 0; i < skill.length; i++) {
                                if(skill[i]  != null ){
                                    if(i == skill.length-1){
                                        out.println(skill[i]);
                                    }else{
                                        out.println(skill[i]+",");
                                    }
                                }

                            }
                        }
                    %>
                </p>

            </div>
            <div class="col s6 offset-s3">
                <p>
                    <%
                        if (languaje != null && languaje.length != 0) {
                            out.println("Los lenguajes que manejas son: ");
                            for (int i = 0; i < languaje.length; i++) {
                                if(languaje[i]  != null ){
                                    if(i == languaje.length-1){
                                        out.println(languaje[i]);
                                    }else{
                                        out.println(languaje[i]+",");
                                    }
                                }

                            }
                        }
                    %>
                </p>

            </div>
            <a class="col s6 offset-s3 btn waves-effect waves-light" href="formulario.jsp">Regresar
                <i class="material-icons right">arrow_back</i>
            </a>

        </div>


    </div>
</div>
<jsp:include page="./shared/footer.jsp" />
<!--JavaScript at end of body for optimized loading-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="./js/materialize.js"></script>
</body>
</html>
