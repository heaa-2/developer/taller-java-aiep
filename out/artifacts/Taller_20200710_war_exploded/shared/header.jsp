<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.jsp" class="brand-logo">
        <img src="./img/logo_white_official.png" width="200"/>
    </a>
        <ul class="right hide-on-med-and-down">
            <li><a href="index.jsp">Tabla Imagenes</a></li>
            <li><a href="formulario.jsp">Formulario</a></li>
            <li><a href="math.jsp">Ecuaciones</a></li>
        </ul>
        <ul id="nav-mobile" class="sidenav">
            <li><a href="index.jsp">Tabla Imagenes</a></li>
            <li><a href="formulario.jsp">Formulario</a></li>
            <li><a href="math.jsp">Ecuaciones</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
