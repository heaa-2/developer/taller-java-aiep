<%--
  Created by IntelliJ IDEA.
  User: efraingaray
  Date: 17-06-20
  Time: 20:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="./shared/head.jsp" />
<body>
<jsp:include page="./shared/header.jsp" />
<div class="section no-pad-bot" id="index-banner">
    <div class="container">


    </div>
</div>
<jsp:include page="./shared/footer.jsp" />
<!--JavaScript at end of body for optimized loading-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="./js/materialize.js"></script>
<script>
    (function($){
        $(function(){

            $('.sidenav').sidenav();
            $('select').formSelect();
        }); // end of document ready
    })(jQuery); // end of jQuery name space

</script>
</body>
</html>
